package server;


import compute.Compute;
import compute.Pair;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.net.Socket;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * This class represents a Server used by the LoadBalancer to process compute tasks.
 * <br>
 * Clients request {@link ComputeEngine}-stubs for remote code
 * execution, that are provided by the ComputeServer vai RMi.
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeServer {

    private String iAddress;
    private Socket socket;
    private boolean listening;
    private HashMap<Compute, Compute> connections;

    private ExecutorService executor;

    private Thread handler;
    private BufferedReader sysIn;

    private ObjectInputStream in;
    private ObjectOutputStream out;

    public ComputeServer(String iAddress) throws IOException {
        this.iAddress = iAddress;
        this.socket = new Socket(iAddress, 5555);
        this.listening = true;
        this.connections = new HashMap<>();
        this.out = new ObjectOutputStream(this.socket.getOutputStream());
        this.in = new ObjectInputStream(this.socket.getInputStream());
        this.executor = Executors.newCachedThreadPool();
    }

    /**
     * Starts the whole Server and creates a request Handler for Server-Admin-Input.
     */
    public void work() throws IOException {
        System.out.println("[Server]: ComputeServer started");

        // Creates handler for user input
        createHandler();
        executor.submit(handler);

        // Wait for clients
        try {
            while (listening) {
                String cmd = (String) this.in.readObject();
                switch (cmd) {
                    case "Request":
                        Pair<Compute, Compute> compute = createEngine();
                        this.connections.put(compute.getFirst(), compute.getSecond());
                        this.out.writeObject(compute.getFirst());
                        System.out.println("[Server]: Ready for execution");
                        break;
                    case "Done":
                        Thread.sleep(50);
                        System.out.println("[Server]: Computation finished");
                        Compute stub = (Compute) this.in.readObject();
                        removeEngine(stub);
                        break;
                    case "Stats":
                        this.out.writeObject("Stats");
                        this.out.writeObject(getStats());
                        break;
                }
            }
        }
        catch (Exception ex) {
            if(ex instanceof java.net.SocketException) {
                System.out.println("[Server]: Closing Server...");
                shutdown();
            }
            else ex.printStackTrace();
        }
    }

    /**
     * Creates a new Compute stub, that is used for processing.
     * @return Compute Stub with corresponding object
     * @throws RemoteException
     */
    private Pair<Compute, Compute> createEngine() throws RemoteException {
        String policyPath = System.getProperty("user.dir");
        policyPath = policyPath.replace("\\", "/");
        policyPath += "/src/main/java/server/server.policy";
        System.setProperty("java.security.policy", policyPath);

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        System.setProperty("java.rmi.server.hostname", iAddress);
        String name = "Compute";
        Compute engine = new ComputeEngine();

        System.out.println("[Server]: Exporting new compute object " + engine);
        Compute stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind(name, stub);

        return new Pair<>(stub, engine);
    }

    /**
     * Removes a exported Stub.
     * @param stub Compute Stub that needs to be removed.
     * @throws NoSuchObjectException
     */
    public void removeEngine(Compute stub) throws NoSuchObjectException {
        Compute engine = this.connections.get(stub);
        System.out.println("[Server]: Removing compute object " + engine);
        UnicastRemoteObject.unexportObject(engine, true);
        this.connections.remove(stub);
    }

    /**
     * Handler that processes user-input on the server.
     * <br>
     * With "shutdown" the user can close the server. <br>
     */
    private void createHandler() {
        handler = new Thread(() -> {
            try {
                sysIn = new BufferedReader(new InputStreamReader(System.in));
                while(!sysIn.ready() && listening) {
                    Thread.sleep(100);
                }
                if (listening) {
                    if (sysIn.readLine().equals("shutdown")) {
                        shutdown();
                    }
                }
            } catch (Exception ex) {ex.printStackTrace();}
        });
    }

    public double getCpuUsage()  {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
            AttributeList list = mbs.getAttributes(name, new String[]{"ProcessCpuLoad"});
            if (list.isEmpty()) return Double.NaN;
            Attribute att = (Attribute) list.get(0);
            Double value = (Double) att.getValue();
            // If no measuerements are found
            if (value == -1.0) return Double.NaN;
            // Return CPU usage in percent
            return ((int) (value * 1000) / 10.0);
        }
        catch (Exception ex) {
            return Double.NaN;
        }
    }

    private String getStats() {
        Runtime runtime = Runtime.getRuntime();
        NumberFormat format = NumberFormat.getInstance();
        StringBuilder stats = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long used = totalMemory - freeMemory;
        stats.append("  CPU---");
        stats.append("    Usage: ").append(getCpuUsage()).append(" %---");
        stats.append("  Memory---");
        stats.append("    Used: ").append(format.format(used / 1000)).append(" Mb---");
        stats.append("    Allocated: ").append(format.format(totalMemory / 1000)).append(" Mb---");
        stats.append("    Max: ").append(format.format((maxMemory / 1000) / 1000)).append(" Gb---");
        return stats.toString();
    }

    private void shutdown() {
        try {
            listening = false;
            in.close();
            executor.shutdown();
            handler.join(1000);
            handler.interrupt();
            for (Compute stub: connections.keySet()) {
                removeEngine(stub);
            }
            try {
                this.out.writeObject("Shutdown");
            } catch (Exception ex) {}
            socket.close();
        } catch (Exception ex) { }
        System.exit(0);
    }

    /**
     * Starts the Server via command line.
     * @param args Server IP-address
     */
    public static void main(String[] args) {
        try {
            System.out.println("[Server]: IP-Address - " + args[0]);
            System.setProperty("java.rmi.server.hostname",args[0]);
            //Registry registry = LocateRegistry.createRegistry(1099);
            ComputeServer server = new ComputeServer(args[0]);
            server.work();
        } catch (Exception e) {
            System.err.println("ComputeServer exception:");
            e.printStackTrace();
        }
    }
}
