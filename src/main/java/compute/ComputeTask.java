package compute;

/**
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
public enum ComputeTask {
    PI(0),
    FIBONACCI(1);

    private int val;

    ComputeTask(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
