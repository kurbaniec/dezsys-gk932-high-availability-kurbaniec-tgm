package compute;

import java.io.Serializable;

/**
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
public class MetaData implements Serializable {
    private ComputeTask task;
    private long param;

    public MetaData(ComputeTask task, long param) {
        this.task = task;
        this.param = param;
    }

    public long getWeight() {
        long weight = 10;
        switch (task) {
            case PI:
                weight = (long) Math.pow(weight, String.valueOf(param).length());
                break;
            case FIBONACCI:
                weight = (long) Math.pow(weight, String.valueOf(param).length());
                break;
        }
        return weight;
    }

    public ComputeTask getTask() {
        return task;
    }

    public void setTask(ComputeTask task) {
        this.task = task;
    }

    public long getParam() {
        return param;
    }

    public void setParam(long param) {
        this.param = param;
    }
}
