/**
 * Task to compute a Fibonacci number.
 * <br>
 * Based on this <a href="https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Fibonacci_Number_Program#Recursive_version_2">code</a>.
 * <br<
 * @author Kacper Urbaniec
 */

package client;

import compute.Task;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Fibonacci implements Task<BigInteger>, Serializable {

    private static final long serialVersionUID = 228L;

    /** Wanted sequence n of the Fibonacci Number */
    private final long n;

    /**
     * Construct a task to calculate the Fibonacci number to the specified
     * sequence n.
     */
    public Fibonacci(long n) {
        this.n = n;
    }

    /**
     * Calculate Fibonacci number.
     */
    public BigInteger execute() {
        return computeFibonacci(n);
    }


    /**
     * Calculate Fibonacci number through recursion.
     * <br>
     * Source: <a href="https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Fibonacci_Number_Program#Recursive_version_2">link</a>
     */
    public BigInteger computeFibonacci(long n)
    {
        if (n <= 0)
        {
            return BigInteger.valueOf(0);
        }

        else return computeFibonacci(n,BigInteger.valueOf(1),BigInteger.valueOf(0));

    }

    /**
     * Recursive Fibonacci number compute.
     * <br>
     * Source: <a href="https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Fibonacci_Number_Program#Recursive_version_2">link</a>
     */
    private BigInteger computeFibonacci(long n, BigInteger eax, BigInteger ebx)
    {
        n--;

        if (n == 0)
        {
            eax = eax.add(ebx);
            return eax;
        }

        else return computeFibonacci(n,ebx,eax.add(ebx));
    }

}
