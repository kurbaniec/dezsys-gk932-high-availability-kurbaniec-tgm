package client;

import compute.Compute;
import compute.ComputeTask;
import compute.MetaData;
import compute.Task;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Starts the client program, in which the client request an stub to execute
 * code remotely.
 * <br>
 * The program is command line based, three parameters are required:
 * <br>
 *     1. Server IP-Address <br>
 *     2. Task name or number <br>
 *     3. Parameter of the task <br>
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class ComputeClient  {

    public static void main(String[] args) {
        // Set policy file
        String policyPath = System.getProperty("user.dir");
        policyPath = policyPath.replace("\\", "/");
        policyPath += "/src/main/java/client/client.policy";
        System.setProperty("java.security.policy", policyPath);

        // Check security
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        // Send Compute request to server to get a stub
        try {
            Socket socket = new Socket(args[0], 4444);
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            ObjectOutputStream outMeta = new ObjectOutputStream(socket.getOutputStream());
            // Get task from user input
            Task task = new Pi(4);
            MetaData meta = new MetaData(ComputeTask.PI, 4);
            try {
                int digits = Integer.parseInt(args[2]);
                if (args[1].matches("-?\\d+(\\.\\d+)?")) {
                    int taskNr = Integer.parseInt(args[1]);
                    if (taskNr == 0) {
                        task = new Pi(digits);
                        meta = new MetaData(ComputeTask.PI, digits);
                    } else if (taskNr == 1) {
                        task = new Fibonacci(digits);
                        meta = new MetaData(ComputeTask.FIBONACCI, digits);
                    }
                }
                else {
                    if (args[1].equals("Pi")) {
                        task = new Pi(digits);
                        meta = new MetaData(ComputeTask.PI, digits);
                    } else if (args[1].equals("Fibonacci")) {
                        task = new Fibonacci(digits);
                        meta = new MetaData(ComputeTask.FIBONACCI, digits);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("Something went wrong\n" + ex + "\nUsing Task Pi(4)");
            }
            // Send metadata about a task to sever — needed for weighted distribution
            outMeta.writeObject(meta);

            Object o = in.readObject();
            Compute comp = (Compute) o;

            Object computedOutput = comp.executeTask(task);
            System.out.println(computedOutput);
            out.println("Done");
        } catch (Exception e) {
            System.err.println("Oops, something went wrong!");
            //e.printStackTrace();
        }
    }

}
