package loadbalancer.worker;

import compute.Compute;
import loadbalancer.LoadBalancer;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * The clientWorker represents a session for a computation task.
 * @author Kacper Urbaniec
 * @version 2019-12-07
 */
public class ClientWorker extends Thread {
    private LoadBalancer callback;
    private ServerWorker engine;
    private Compute stub;
    private Socket socket;
    private ObjectOutputStream out;
    private BufferedReader reader;
    private ObjectInputStream in;
    private PrintWriter writer;

    public ClientWorker(LoadBalancer callback, ServerWorker engine, Compute stub, Socket socket,
                        ObjectOutputStream out, ObjectInputStream in) throws IOException {
        this.callback = callback;
        this.engine = engine;
        this.stub = stub;
        this.socket = socket;
        this.out = out;
        this.in = in;
        this.writer = new PrintWriter(socket.getOutputStream(), true);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public void run() {
        try {
            this.out.writeObject(stub);
            this.reader.readLine();
            this.callback.taskFinished(this, engine, stub);
        } catch (IOException e) {
            try {
                this.callback.taskFinished(this, engine, stub);
            } catch (Exception ex) {ex.printStackTrace();}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ServerWorker getEngine() {
        return engine;
    }

    // TODO client shutdown
    public void shutdown() {
        try {
            socket.close();
        }
        catch (Exception ex) {}
    }

}
