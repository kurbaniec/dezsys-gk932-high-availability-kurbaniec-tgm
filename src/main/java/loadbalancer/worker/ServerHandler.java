package loadbalancer.worker;

import loadbalancer.LoadBalancer;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * The ServerHandler is responsible registering new Compute Servers.
 * @author Kacper Urbaniec
 * @version 2019-12-07
 */
public class ServerHandler extends Thread {

    private LoadBalancer callback;
    private ServerSocket serverSocket;
    private boolean listening;

    public ServerHandler(LoadBalancer callback) throws IOException {
        this.callback = callback;
        this.serverSocket = new ServerSocket(5555);
        this.listening = true;
    }

    @Override
    public void run() {

        while (listening) {
            try {
                ServerWorker engine = new ServerWorker(callback, serverSocket.accept());
                this.callback.addEngine(engine);
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        listening = false;
        try {
            serverSocket.close();
        } catch (Exception ex) {}
    }
}
