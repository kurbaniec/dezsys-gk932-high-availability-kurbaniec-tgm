package loadbalancer.worker;

import compute.Compute;
import compute.MetaData;
import loadbalancer.LoadBalancer;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;

/**
 * The ServerWorker represents one with the LoadBalancer connected Compute Server.
 * @author Kacper Urbaniec
 * @version 2019-12-07
 */
public class ServerWorker extends Thread {
    private LoadBalancer callback;
    private Socket socket;
    private String iAddress;
    private boolean listening;
    private long connections;
    private HashMap<Compute, Long> weight;
    private ObjectOutputStream out;
    private BufferedReader reader;
    private ObjectInputStream in;
    private PrintWriter writer;
    private Compute stub;
    private String stats;

    public ServerWorker(LoadBalancer callback, Socket socket) throws IOException {
        this.callback = callback;
        this.socket = socket;
        this.listening = true;
        this.connections = 0;
        this.weight = new HashMap<>();
        this.in = new ObjectInputStream(socket.getInputStream());
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new PrintWriter(socket.getOutputStream(), true);
        this.stub = null;
        this.stats = null;
    }

    /**
     * Is called when the engine is chosen to process a compute task from a client.
     * @param task The processing task
     * @return RMI stub, that the client can use for processing
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public Compute taskInit(MetaData task) throws IOException, ClassNotFoundException, InterruptedException {
        this.out.writeObject("Request");
        while (stub == null) {
            Thread.sleep(100);
        }
        Compute returnStub = stub;
        stub = null;
        connections++;
        addWeight(returnStub, task.getWeight());
        return returnStub;
    }

    /**
     * Is called when the compute task is finished.
     * @param stub RMI stub, that the server needs to unexport.
     */
    public void taskDone(Compute stub) {
        try {
            this.out.writeObject("Done");
            this.writer.flush();
            this.out.writeObject(stub);
            this.removeWeight(stub);
            connections--;
            removeWeight(stub);
        } catch (Exception ex) {
            //ex.printStackTrace();
            this.removeWeight(stub);
            connections--;
            removeWeight(stub);
        }
    }

    /**
     * Gets CPU and Memory metrics from the Compute Server.
     * @return CPU and Memory metrics
     * @throws IOException
     * @throws InterruptedException
     */
    public String getStats() throws IOException, InterruptedException {
        this.out.writeObject("Stats");
        while (stats == null) {
            Thread.sleep(100);
        }
        String returnStats = stats;
        stats = null;
        return returnStats;
    }


    @Override
    public void run() {
        while (listening) {
            try {
                Object msg = this.in.readObject();
                if (msg instanceof Compute) {
                    stub = (Compute) msg;
                } else if (msg instanceof String && msg.equals("Stats")) {
                    stats = (String) this.in.readObject();
                } else if (msg instanceof String && msg.equals("Shutdown")) {
                    callback.removeEngine(this);
                }
            } catch (IOException | ClassNotFoundException e) {
                callback.removeEngine(this);
            }
        }
    }

    public void shutdown() {
        this.listening = false;
        try {
            socket.close();
        } catch (Exception ex) {}

    }

    public long getWeight() {
        return this.weight.values().stream().mapToLong(Long::longValue).sum();
    }

    public void addWeight(Compute stub, long weight) {
        this.weight.put(stub, weight);
    }

    public void removeWeight(Compute stub) {
        this.weight.remove(stub);
    }

    public long getConnections() {
        return connections;
    }
}
