package loadbalancer.worker;

import compute.MetaData;
import loadbalancer.LoadBalancer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The ClientHandler is responsible for accepting new computation task from the clients.
 * @author Kacper Urbaniec
 * @version 2019-12-07
 */
public class ClientHandler extends Thread {

    private LoadBalancer callback;
    private ServerSocket clientSocket;
    private boolean listening;

    private Socket connection;

    private ObjectOutputStream clientOut;
    private ObjectInputStream clientIn;

    public ClientHandler(LoadBalancer callback) throws IOException {
        this.callback = callback;
        this.clientSocket = new ServerSocket(4444);
        this.listening = true;
    }

    @Override
    public void run() {

        while (listening) {
            try {
                Socket connection = clientSocket.accept();
                clientOut = new ObjectOutputStream(connection.getOutputStream());
                clientIn = new ObjectInputStream(connection.getInputStream());
                MetaData task = (MetaData) clientIn.readObject();
                this.callback.balance(connection, task);
            } catch (IOException | ClassNotFoundException e) {
                // e.printStackTrace();
            }
        }

    }

    public void shutdown() {
        listening = false;
        try {
            clientSocket.close();
        } catch (Exception ex) {}
    }

    public ObjectOutputStream getClientOut() {
        return clientOut;
    }

    public ObjectInputStream getClientIn() {
        return clientIn;
    }
}
