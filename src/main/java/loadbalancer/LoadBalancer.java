package loadbalancer;

import compute.Compute;
import compute.MetaData;
import loadbalancer.method.LeastConnection;
import loadbalancer.method.LoadBalancerMethod;
import loadbalancer.method.RoundRobin;
import loadbalancer.method.WeightedRoundRobin;
import loadbalancer.worker.ClientHandler;
import loadbalancer.worker.ClientWorker;
import loadbalancer.worker.ServerWorker;
import loadbalancer.worker.ServerHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class represents a Loadbalancer-Server for computation request.
 * <br>
 * Clients request {@link server.ComputeEngine}-stubs for remote code execution. Every {@link server.ComputeServer}
 * contains one or multiple {@link server.ComputeEngine} and is responsible for exporting the engine into the
 * RMI-registry.
 * <br>
 * @author Kacper Urbaniec
 * @version 16.03.2019
 */
public class LoadBalancer {
    private ArrayList<ServerWorker> engines;
    private ArrayList<ClientWorker> clients;
    private ExecutorService executor;
    private String iAddress;

    private Thread handler;
    private boolean listening = true;
    private int index = 0;
    private LoadBalancerMethod lb;

    private ClientHandler clientHandler;
    private ServerHandler serverHandler;

    public LoadBalancer(String iAddress) throws IOException {
        this.executor = Executors.newCachedThreadPool();
        this.iAddress = iAddress;
        this.engines = new ArrayList<>();
        this.clients = new ArrayList<>();
        this.lb = new WeightedRoundRobin();
    }

    /**
     * Balance incoming computation requests.
     * @param connection
     * @param task
     */
    public synchronized void balance(Socket connection, MetaData task) {
        lb.balance(this, connection, task);
    }

    public synchronized void taskFinished(ClientWorker client, ServerWorker engine, Compute stub) throws InterruptedException {
        removeClient(client);
        lb.taskFinished(engine, stub);
    }

    /**
     * Start the LoadBalancer-Server.
     * @throws IOException
     */
    public void work() throws IOException {
        serverHandler = new ServerHandler(this);
        clientHandler = new ClientHandler(this);
        executor.submit(serverHandler);
        executor.submit(clientHandler);

        createHandler();
        executor.submit(handler);
        System.out.println("[LoadBalancer]: LoadBalancer started");
    }

    /**
     * Handler that processes user-input on the server.
     * <br>
     * With "shutdown" the user can close the server. <br>
     * With "method" the user can change the balancing method. <br>
     *   Viable optiosns are: rr (Round Robin), wrr (Weighted Round Robin) and lc (Least connection) <br>
     * With "status" the user can see loadbalancer and compunte engine details
     */
    private void createHandler() {
        handler = new Thread(() -> {
            try {
                BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
                String input;
                while (!(input = sysIn.readLine()).equals("shutdown")) {
                    if (input.startsWith("method") && input.split(" ").length == 2) {
                        LoadBalancerMethod method = lb.method(input.split(" ")[1]);
                        if (method != null) lb = method;
                    }
                    else if(input.equals("status")) {
                        System.out.println("[LoadBalancer]: Status");
                        System.out.println("[LoadBalancer]:   Engine count: " + engines.size());
                        System.out.println("[LoadBalancer]:   Balancing method: " + lb.name());
                        for (ServerWorker engine: engines) {
                            System.out.println("[Engine " + LoadBalancer.simpleThreadName(engine) + "]: Status");
                            System.out.println("[Engine " + LoadBalancer.simpleThreadName(engine) + "]:   " +
                                   "Connections: " +  engine.getConnections());
                            System.out.println("[Engine " + LoadBalancer.simpleThreadName(engine) + "]:   " +
                                    "Weight: " + engine.getWeight());
                            String[] stats = engine.getStats().split("---");
                            for (String stat: stats) {
                                System.out.println("[Engine " + LoadBalancer.simpleThreadName(engine) + "]: " + stat);
                            }

                        }

                    }
                    Thread.sleep(100);
                }
                listening = false;
                serverHandler.shutdown();
                clientHandler.shutdown();
                executor.shutdown();
                sysIn.close();
                System.exit(0);
            } catch (Exception ex) {ex.printStackTrace();}
        });
    }

    public synchronized void addEngine(ServerWorker engine) {
        System.out.println("[LoadBalancer]: New engine [" + LoadBalancer.simpleThreadName(engine) + "] registered");
        engines.add(engine);
        executor.submit(engine);
    }

    public synchronized void removeEngine(ServerWorker engine) {
        System.out.println("[LoadBalancer]: Engine [" + LoadBalancer.simpleThreadName(engine) + "] unregistered");
        engine.shutdown();
        engines.remove(engine);
    }

    public synchronized void addClient(ClientWorker client) {
        System.out.println("[LoadBalancer]: Received a new task [" + LoadBalancer.simpleThreadName(client) + "]");
        System.out.println("[LoadBalancer]: Executing task [" + LoadBalancer.simpleThreadName(client) + "]" +
                " on engine [" + LoadBalancer.simpleThreadName(client.getEngine()) + "]");
        clients.add(client);
        executor.submit(client);
    }

    public synchronized void removeClient(ClientWorker client) {
        System.out.println("[LoadBalancer]: Finished task [" + LoadBalancer.simpleThreadName(client) + "]" +
                " on engine [" + LoadBalancer.simpleThreadName(client.getEngine()) + "]");
        client.shutdown();
        clients.remove(client);
    }

    public static String simpleThreadName(Object thread) {
        String name = thread.toString().substring(thread.toString().indexOf("Thread-")+7);
        return name.substring(0, name.length()-8);
    }

    public ArrayList<ServerWorker> getEngines() {
        return engines;
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    /**
     * Starts the LoadBalancer via command line.
     * <br>
     * At the beginning a RMI-Registry is created. For correct executing the server
     * IP-address is needed.
     * @param args Server IP-address
     */
    public static void main(String[] args) {
        try {
            System.out.println("[LoadBalancer]: IP-Adress - " + args[0]);
            System.setProperty("java.rmi.server.hostname",args[0]);
            Registry registry = LocateRegistry.createRegistry(1099);
            LoadBalancer loadBalancer = new LoadBalancer(args[0]);
            loadBalancer.work();
        } catch (Exception e) {
            System.err.println("ComputeServer exception:");
            e.printStackTrace();
        }
    }
}