package loadbalancer.method;

import compute.Compute;
import compute.MetaData;
import loadbalancer.LoadBalancer;
import loadbalancer.worker.ServerWorker;

import java.net.Socket;

/**
 * Interface that defines, what a Load-Balancing method needs.
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
public interface LoadBalancerMethod {
    void balance(LoadBalancer lb, Socket connection, MetaData task);

    void taskFinished(ServerWorker engine, Compute stub);

    String name();

    default LoadBalancerMethod method(String name) {
        switch (name) {
            case "rr":
            case "Round Robin":
                return new RoundRobin();
            case "wrr":
            case "Weighted Round Robin":
                return new WeightedRoundRobin();
            case "lc":
            case "Least Connection":
                return new LeastConnection();
        }
        return null;
    }
}
