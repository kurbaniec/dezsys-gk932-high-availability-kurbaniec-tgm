package loadbalancer.method;

import compute.Compute;
import compute.MetaData;
import loadbalancer.LoadBalancer;
import loadbalancer.worker.ClientWorker;
import loadbalancer.worker.ServerWorker;

import java.net.Socket;
import java.util.ArrayList;


/**
 * Implementation of Balancing-Method Round Robin.
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
public class RoundRobin implements LoadBalancerMethod {
    private String name;
    private int index;

    public RoundRobin() {
        this.name = "Round Robin";
        this.index = 0;
    }

    @Override
    public void balance(LoadBalancer lb, Socket connection, MetaData task) {
        ArrayList<ServerWorker> engines = lb.getEngines();
        int engineSize = engines.size();
        if (index >= engineSize) {
            index = 0;
        }
        if (engineSize != 0) {
            try {
                ServerWorker engine = engines.get(index);
                Compute stub = engine.taskInit(task);
                lb.addClient(new ClientWorker(lb, engine, stub, connection, lb.getClientHandler().getClientOut(),
                        lb.getClientHandler().getClientIn()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            index++;
        }
        else System.err.println("[Server]: No active Compute Engines");

    }

    @Override
    public void taskFinished(ServerWorker engine, Compute stub) {
        engine.taskDone(stub);
    }

    @Override
    public String name() {
        return name;
    }
}
