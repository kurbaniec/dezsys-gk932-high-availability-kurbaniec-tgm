package loadbalancer.method;



import compute.Compute;
import compute.MetaData;
import loadbalancer.LoadBalancer;
import loadbalancer.worker.ClientWorker;
import loadbalancer.worker.ServerWorker;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Implementation of Balancing-Method Least Connection.
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */

public class LeastConnection implements LoadBalancerMethod {
    private String name;

    public LeastConnection() {
        this.name = "Least Connection";
    }

    @Override
    public void balance(LoadBalancer lb, Socket connection, MetaData task) {
        ArrayList<ServerWorker> engines = lb.getEngines();
        if (engines.size() > 0) {
            int index = 0;
            long min = engines.get(0).getConnections();
            for (int i = 1; i < engines.size(); i++) {
                long test = engines.get(i).getConnections();
                if (test < min) {
                    index = i;
                    min = test;
                }
            }
            try {
                ServerWorker engine = engines.get(index);
                Compute stub = engine.taskInit(task);
                lb.addClient(new ClientWorker(lb, engine, stub, connection, lb.getClientHandler().getClientOut(),
                        lb.getClientHandler().getClientIn()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        else System.err.println("[Server]: No active Compute Engines");

    }

    @Override
    public void taskFinished(ServerWorker engine, Compute stub) {
        engine.taskDone(stub);
    }

    @Override
    public String name() {
        return name;
    }
}
