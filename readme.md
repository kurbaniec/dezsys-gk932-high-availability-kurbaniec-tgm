# GK9.3.2 Middleware Engineering "High Availability"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Als Basis für das Projekt habe ich den Load-Balancer vom letzten Jahr aus der Übung bezüglich RMI (gk833) genommen. Diesen habe ich aber umgeschrieben, damit er besser an die Bedürfnisse passt (klare Trennung zwischen Load-Balancer-Server und Computer-Server Instanzen).

Grundsätzlich gibt es drei Aktoren: Load-Balancer, Compute-Server und Compute-Client.

Der Load-Balancer registriert Compute-Server, falls diese gestartet werden. Wenn der Client eine Anfrage an den Load-Balancer herstellt (z.B. Kalkulation der 1000ten Stelle von Pi),  wählt dieser einen geeigneten Compute-Server, beeinflusst durch die aktive Load-Balancing Methodik, und fordert von ihm einen RMI-Stub an, welchen der Load-Balancer an den Client sendet. Dieser verwendet den Stub, um damit seine Kalkulation zu berechnen, wobei die Berechnung selbst, am gewählten Compute-Server mithilfe von RMI erfolgt.  Ist der Client fertig, sendet dieser den Stub zurück an den Load-Balancer. Dieser berechnet die aktiven Verbindungen von Clients und Gewichtungen neu und sagt dem Compute-Server, von dem der Stub kommt, dass er diesen entfernen soll, da die Berechnungen fertig sind.

Das Load-Balancing im Programm wird jedes mal aufgerufen, wenn ein Client eine neue Anfrage sendet. Wie dieses abgehandelt wird, hängt von der gewählten Methodik aus. Bei der Implementierung habe ich ein Interface `LoadBalancerMethod` erstellt, was als Blaupause für Load-Balancing Methoden dient.

```java
public interface LoadBalancerMethod {
    void balance(LoadBalancer lb, Socket connection, MetaData task);

    void taskFinished(ServerWorker engine, Compute stub);
    ...
}
```

Die Methode `balance` wird bei jeder neuen Anfrage aufgerufen und führt das Umverteilen auf einen bestimmten Compute-Server aus. Der Parameter `task` beinhaltet konkrete Anfragedaten (z.B. Kalkulation von Pi, 1000 Stellen). Das Benutzen eines Interfaces erlaubt es zur Laufzeit die Balancing Strategie zu ändern. Die Method `taskFinished` wird aufgerufen, um den Load-Balancer zu informieren, dass eine Client Anfrage fertig ist.

Konkret habe ich drei Strategien implementiert - Round Robin, Weighted Round Robin, Least Connection - wobei ich auf Round Robin nicht eingehen werde.

Die konkrete Implementierung für Least Connection sieht folgendermaßen aus.

```java
public class LeastConnection implements LoadBalancerMethod {
    ...
    @Override
    public void balance(LoadBalancer lb, Socket connection, MetaData task) {
        ArrayList<ServerWorker> engines = lb.getEngines();
        if (engines.size() > 0) {
            int index = 0;
            long min = engines.get(0).getConnections();
            for (int i = 1; i < engines.size(); i++) {
                long test = engines.get(i).getConnections();
                if (test < min) {
                    index = i;
                    min = test;
                }
            }
            try {
                ServerWorker engine = engines.get(index);
                Compute stub = engine.taskInit(task);
                lb.addClient(new ClientWorker(lb, engine, stub, connection, lb.getClientHandler().getClientOut(),
                        lb.getClientHandler().getClientIn()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        else System.err.println("[Server]: No active Compute Engines");
    }
    ...
}
```

Am Anfang werden aus dem Load-Balancer Objekt alle Compute-Server mittels `lb.getEngines()` geholt. Über diese wird durchiteriert und geschaut, welcher Server die wenigsten Verbindungen hat. Dieser wird dann ausgewählt, und mittels `engine.taskInit(task)` wird der RMI-Stub vom Server angefordert, der dem Client zurückgegeben wird.

Die Load-Balancing Methode Weighted Round Robin funktioniert ganz anderes. 

```java
public class WeightedRoundRobin implements LoadBalancerMethod {
    private int index;
	...
    @Override
    public void balance(LoadBalancer lb, Socket connection, MetaData task) {
        ArrayList<ServerWorker> engines = lb.getEngines();
        int engineSize = engines.size();
        if (index >= engineSize) {
            index = 0;
        }
        if (engineSize != 0) {
            try {
                // Get weight of engine, that would be chosen with typical round robin
                // The value - 5000 - is subtracted as a form of tolerance
                long weight = engines.get(index).getWeight() - 5000;
                for (int i = 0; i < engineSize; i++) {
                    if (engines.get(i).getWeight() < weight) {
                        index = i;
                    }
                }
                // Continue with typical round robin
                ServerWorker engine = engines.get(index);
                Compute stub = engine.taskInit(task);
                lb.addClient(new ClientWorker(lb, engine, stub, connection, lb.getClientHandler().getClientOut(),
                        lb.getClientHandler().getClientIn()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            index++;
        }
        else System.err.println("[Server]: No active Compute Engines");
    }
    ...
}
```

Grundsätzlich, bei gleicher Verteilung, funktioniert diese Methode wie Round Robin: die Auswahl des Servers ist eine einfache Iteration, die sich wiederholt. Bei Weighted Round Robin schaut man aber auf die Auslastung der Server, und wählt den derzeit optimalsten Server, wenn ungleiche Bedingungen herrschen. Ungleiche Bedingungen können unterschiedlich starke Server sein, oder wie in meinen Fall, unterschiedlich aufwendige Client-Anfragen. Jedes mal wenn der Load-Balancer eine Anfrage einem Server zuteilt, addiert er ihm eine Gewichtung hinzu. Rechenintensivere Aufgaben haben logischerweise eine höhere Gewichtung. Die Berechnung der Gewichtung erfolgt im meinen Fall durch die Anfragen-Daten des Clients und sind sehr einfach gestaltet.

```java
public class MetaData implements Serializable {
    private ComputeTask task; //e.g. PI -> Calculate Pi
    private long param;		  //e.g. 10 -> 10th digit of Pi
	...
    public long getWeight() {
        long weight = 10;
        switch (task) {
            case PI:
                weight = (long) Math.pow(weight, String.valueOf(param).length());
                break;
            case FIBONACCI:
                weight = (long) Math.pow(weight, String.valueOf(param).length());
                break;
        }
        return weight;
    }
    ...
}
```

Jetzt wird bei der Auswahl geschaut, ob es einem Server mit niedrigerer Auslastung gibt, wenn ja, wird dieser  genommen.  Dadurch können die Server besser zugeteilt werden und man vermeidet den Fall, dass ein Server zwei rechenintensive Aufgaben hintereinander bekommt.

## Ausführen

Load-Balancer starten:

```bash
gradle loadBalancer --args="[Load-Balancer IP-Adresse]"
```

Load-Balancer Eingabefunktionen:

* `method [rr|wrr|ls]` - Wechselt die derzeitige Balancing Methodik (Round Robin, Weighted Round Robin, Least Connection)
* `status` - Zeigt alle registrierten Server, Client-Verbindungen und Server-Auslastungen
* `shutdown` - Beendet den Load-Balancer

Compute-Server starten:

```bash
gradle computeServer --args="[Load-Balancer IP-Adresse]"
```

Compute-Server Eingabefunktionen:

* `shutdown` - Beendet den Compute-Server

Client starten:

```bash
gradle computeClient --args="[Server IP-Addresse] [Task-Name oder Nummer] [Task-Parameter]"
// Calulate Pi
gradle computeClient --args="localhost Pi 10" // 10th digit of Pi
gradle computeClient --args="localhost 0 10"  // Valid alternative call
// Fibonacci Task Beispiele
gradle computeClient --args="localhost Fibonacci 10" // 10th Fibonacci number
gradle computeClient --args="localhost 1 10"         // Valid alternative call
```

## Resultate

Zum testen der Belastung habe ich am Load-Balancer eine Funktion erstellt, die die derzeitigen Verbindungen und Auslastungen aller Compute-Server anzeigt. Dazu muss man nur in Command-Line Interface `status` schreiben.

Falls man dies softwareseitig nicht implementieren möchte (mittels Runtime- und JMX-Api) kann man das Programm JConsole verwenden, was bei jeder JDK-Installation mit installiert werden sollte. Das Programm ermöglicht es die Performance gestarteter Java-Anwendungen zu überwachen und managen. Zum Starten sollte es genügen `jconsole`  in die Befehlszeile einzugeben. Danach sollte man sich mit seiner Applikation verbinden können.

![](images/jconsole.PNG)

Zum Testen habe ich zwei verschiedene Client-Anfrage verwendet. 

```
gradle computeClient --args="localhost Pi 1000000" // Intensive task
gradle computeClient --args="localhost Pi 80000" // Normal task
```

Die aufwändige Anfrage ist ideal zum Testen, da sie fast schon blockierend arbeitet, weil sie sehr lange zum abhandeln benötigt. Die normale Anfrage ist nicht trivial, sie braucht um die 10 Sekunden, ist aber bestens geeignet, um mehrere "schwächere" Aufgaben hintereinander starten zu können, ohne das die vorherige schon beendet ist, beim Starten der nächsten.

Ein Test, wo sich Least Connection gleich wie Weighted Round Robin verhält ist, wenn man einen Compute Server startet, zwei aufwändige Anfragen sendet und danach einen weiteren Server startet und drei aufwändige Anfragen sendet. Die ersten zwei Anfragen werden logisch auf dem ersten Compute Server bearbeitet, die nächsten zwei auf dem zweiten Server und die letzte Anfrage auf dem ersten Server. Hier verhalten sich beide Algorithmen gleich und haben eine ähnliche Performance.

#### Least Connection

1 Server , 2 neue Anfragen

![](images/lc1.PNG)

2 Server wird dazugeschaltet, 3 neue Anfragen

![](images/lc2.PNG)

#### Weighted Round Robin

1 Server , 2 neue Anfragen

![](images/wrr1.PNG)

2 Server wird dazugeschaltet, 3 neue Anfragen

![](images/wrr2.PNG)

Anzumerken ist, dass der zusätzliche Speicherverbrauch bei Weighted Round Robin durch Speicherrückstände in der JVM und auf ein noch nicht ausgeführte Garbage Collection zurückzuführen sind.

Der nächste zeigt am besten, wieso Weighted Round Robin besser als Least Connection sein kann. Man startet einen Compute-Server und sendet ihm eine aufwändige Anfrage. Danach startet man einen weiten Server und sendet zwei normale Anfragen. Least Connection weist die zweite normale Anfrage dem ersten Server zu, der gerade eine schwere Aufgabe löst und damit quasi ausgelastet ist. Weighted Round Robin weist beide normalen Anfragen dem zweiten Server hinzu, dieser ist kurzzeitig höher belastet, jedoch effizienter bei der Bearbeitung und schafft einen Ausgleich der Ressourcen. 

### Least Connection

![](images/lc3.PNG)

### Weighted Round Robin

![](images/wrr3.PNG)

Man sieht, dass beide Anfragen zusammengenommen fast gleiche CPU Werte haben. Dies liegt daran, dass die Kalkulation von Pi Nachkommastellen aufwendig auf lange Zeit ist. Somit ist die derzeitige Auslastung nicht abgängig von den Nachkommastellen, die Nachkommastellen beeinflussen nur für wie lange diese Auslastung vorhanden ist. Man sieht jedoch, dass der Arbeitsspeicher besser aufgeteilt ist. Mehr Nachkommastellen bedeuten höheren Speicherbedarf, darum ist es besser eine aufwendigere Aufgabe einem Server zu zuteilen und mehrere Kleine einem anderen.

## Fragestellung für die Dokumentation

Verlgeichen Sie die verwendeten Load Balancing Methoden und stellen Sie diese gegenüber.

Grundsätzlich bin ich bei der Implementierung auf meine gewählten Methoden, Least Connection und Weighted Round Robin, eingegangen. Daher komme ich jetzt zu den Unterschieden. Least Connection würde ich als den eher primitiveren Algorithmus bezeichnen, während Weighted Round Robin meiner Meinung nach, ein effizienteres Verfahren darstellt. Bei Least Connection wird der Server ausgewählt, der gerade die wenigsten Verbindungen hat. Bei Weighted Round Robin schenkt man dagegen mehr Beachtung den verfügbaren Ressourcen bzw. unterteilt die Anfragen in verschiedene Kategorien. Dadurch wählt der Load-Balancer den geeignetsten Server bei jeder Anfrage aus. Was aber ein negativer Aspekt bei Weighted Round Robin ist, dass man den einzelnen Server eine Gewichtung ihrer Rechenleistung geben muss. Hat ein Server eine falsche Gewichtung, kann sich dies negativ auf eine effiziente Umverteilung auswirken. Dies ist auch problematisch, wenn die Gewichtung nicht auf Server Rechenleistung, sondern auf  Anfragen basiert. Das Berechnen der Gewichtung der Anfrage benötigt zusätzliche Rechenleistung am Load-Balancer und kann im Falle einer falschen Berechnung eine nicht funktionelle Umverteilung der Anfragen zur Folge haben.

- Was kann als Gewichtung bei Weighted Round Robin verwendet werden?

  Grundsätzlich können zwei Aspekte zur Gewichtung genommen werden:

  * Server-Rechenleistung

    Jedem Server, der vom Load-Balancer verwaltet wird, wird ein Wert zugeteilt, der seine Rechenleistung darstellt. Wenn ein Server doppelt so stark wie ein anderer, hat er einen doppelt so hohen Wert. Mithilfe dieser Gewichtungen, wird die Last dann so verteilt, dass ein doppelt so starker Server, doppelt so viele Anfragen kriegt wie ein normaler.

  * Komplexität der Anfrage

    Falls der angebotene Service Anfragen annimmt,  die unterschiedlich viel Rechenleistung benötigen, ist es oft sinnvoll jeder Anfrage eine Gewichtung zu geben und die dem bearbeitenden Server zuzuweisen. Bei der nächsten Anfragen wird dann geschaut, welcher Server die kleinste Summe aller ihm zugeteilten Gewichtungen besitzt. Dieser Server bekommt dann die neue Anfrage.
    
    

- Warum stellt die "Hochverfügbarkeit" von IT Systemen in der heutigen Zeit eine sehr wichtige Eigenschaft dar?

  Viele Aspekte des modernen Lebens spielen sich im Internet ab, seien es Finanzleistungen, Onlineshopping oder was anderes. Falls diese Services ausfallen kann es zu verheerenden Problemen  im Falle in der Finanzwelt / medizinischen Anstalten kommen. Aber auch Ausfälle nicht lebensnotwendiger Services (z.B. Streaming) sind schlecht für die Betreiber, weil dann Endnutzer nicht zufrieden sind und dies sich auf schlecht auf die Reputation des Unternehmens auswirkt, was zu Kurs-Einbrüchen führen kann. Weiters garantieren viele Unternehmen, die Cloud-Computing anbieten, eine garantierte Verfügbarkeit ihrer Services. Wenn sie diese nicht einhalten, kann das zu Strafzahlungen führen.

  

- Welche anderen Maßnahmen neben einer Lastverteilung müssen getroffen werden, um die "Hochverfügbarkeit" sicher zu stellen?

  Das ganze System muss eine Fehlertoleranz garantieren. Dabei müssen alle "Single Points of Failure"-Fehler beseitigt werden, also alle Fehler die zu einem Totalkollaps des Systems führen würden. Diese Fehlertoleranz muss sowohl bei Hardware, also auch bei der Software sichergestellt werden. Weiters werden schnelle Fehlererkennungsverfahren benötigt, um im Falle des Ausfall einer Komponente, diese durch eine andere schnell ersetzen zu können. Somit muss das ganze System eine schnelle automatische Wiederherstellungsmöglichkeit besitzen. 

  

- Was versteht man unter "Session Persistenz" und welche Schwierigkeiten ergeben sich damit?

  "Session Persistenz" bezeichnet die Weiterleitung aller Anfragen eines Clients an genau einen Server.  Dadurch, dass der Client immer mit einen Server kommuniziert, kann es zu langsamen Verbindungen kommen, da dieser eine Server im nächsten Moment vielleicht von einer anderen Anfrage ausgelastet wird. Weiters hat der Client keinen Vorteil, wenn der Anbieter eine Server-Farm (viele individuelle Server) besitzt, da er immer mit genau einem Server die ganze Zeit kommuniziert.

  

- Nennen Sie jeweils ein Beispiel, wo Session Persistenz notwendig bzw. nicht notwendig ist.

  Session Persistenz ist absolut notwendig bei Transaktionen (unteilbaren Aktionen), also der z.B. der Bestellprozess in einem Onlineshop. Ich kann nicht einem Server meine Bezahldaten geben und einem anderen Server sagen, er soll die Buchung durchführen, denn dieser verfügt nicht über diese Daten.

  Wo aber Session Persistenz notwendig ist, ist z.B. das Aufrufen einer Website. Wenn ich auf eine andere Unterseite gehe, ist es egal, ob diese von ursprünglichen Server oder einem anderen zurückgegeben wird.  

  

- Welcher Unterschied besteht zwischen einer "server-side" bzw "client-side" Lastverteilungslösung?

  * Server-Side Load Balancing

    Darunter versteht man das "klassische" Load-Balancing. Der Load-Balancer ist ein eigener Server, der die Vermittlung zwischen Client und mehreren Server übernimmt.

    ![](images/sslb.PNG)

    

  * Client-Side Load Balancing

    Client-Side Load Balancing ist ein eher neueres Verfahren, dass durch die Popularität von Microservices entstanden ist. Ein Vorteil ist, dass man keinen eigenen Server als Load-Balancer braucht, da der Client sich am Anfang eine Liste aller Server zu kommen lässt und dann selbst auswählt, welchen er verwenden möchte, z.B. mit Hilfe von Round Robin. 
    
    ![](images/cslb.PNG)

  

- Was versteht man unter dem "Mega-Proxy-Problem"?

  Unter bestimmten Umständen kann die Verwendung von Persistenz basierend auf der IP-Adresse des Clients nicht korrekt durchgeführt werden. In Systemen mit vielen Proxys haben Client-Anfragen häufig unterschiedliche IP-Adressen, auch wenn sie von demselben Client gesendet werden, was zu einer schnellen Vervielfachung von Persistenz-Sitzungen führt, bei denen eine einzelne Sitzung erstellt werden sollte. Dieses Problem wird als "Mega-Proxy-Problem" bezeichnet. Anstelle von IP-basierter Persistenz  können aber HTTP-Cookies verwendet werden, um dies zu verhindern.



## Quellen

* [Initialize ObjectOutputStream Before ObjectInputStream | 07.12.2019](https://stackoverflow.com/questions/8088557/getinputstream-blocks)
* [StreamCorruptedException | 07.12.2019](https://stackoverflow.com/a/2395269)
* [Measure Java Application CPU usage | 08.12.2019](https://stackoverflow.com/a/21962037)
* [Monitor Java Application with JConsole | 08.12.2019](https://docs.oracle.com/javase/7/docs/technotes/guides/management/jconsole.html)
* [Weighted Round Robin | 08.12.2019](https://www.ibm.com/support/knowledgecenter/en/SSCVKV_9.1.1/Campaign/Listeners/WeightedRoundRobin.html)
* [Load-Aware Round Robin | 08.12.2019](https://www.researchgate.net/publication/279289195_A_load-aware_weighted_round-robin_algorithm_for_IEEE_80216_networks)
* [High Availability Kriterien | 09.12.2019](https://www.informatik-aktuell.de/betrieb/verfuegbarkeit/hochverfuegbarkeit-und-downtime-eine-einfuehrung.html)
* [High Availability Kriterien | 09.12.2019](https://www.it-administrator.de/themen/server_client/grundlagen/98988.html)
* [Session Persistence | 09.12.2019](https://www.nginx.com/resources/glossary/session-persistence/)
* [Server Side / Client Side Load Balancing | 09.12.2019](https://gooroo.io/GoorooTHINK/Article/17367/Spring-Cloud-and-Netflix-Ribbon-Clientside-Load-Balancing/28985)
* [Mega Proxy Problem | 09.12.2019](https://dingyuliang.me/load-balance-persistence-methods-layer-7/)